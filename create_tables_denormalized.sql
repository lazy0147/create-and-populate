START TRANSACTION;
CREATE DATABASE denormalized_grabmyfood;
USE denormalized_grabmyfood;
COMMIT;
create table User
(
    UserId       bigint auto_increment
        primary key,
    Username     varchar(60) not null,
    Password     varchar(60) not null,
    Phone_Number varchar(10) not null,
    CreditCard   varchar(16) null,
    constraint User_UserId_uindex
        unique (UserId)
);
create table Restaurant
(
    RestaurantId         bigint auto_increment
        primary key,
    Name                 varchar(60)    not null,
    PhoneNumber          varchar(10)    null,
    TitleOfListing       varchar(50)    not null,
    DescriptionOfListing varchar(50)    null,
    PriceOfListing       smallint       not null,
    Latitude             decimal(10, 5) not null,
    Longitude            decimal(10, 5) not null,
    Branch				 varchar(255)   null,
    constraint Restaurant_RestaurantId_uindex
        unique (RestaurantId),
    constraint Restaurant__UniqueListing_uindex
        unique (TitleOfListing, DescriptionOfListing, Branch)
);
create table Location
(
    LocationId bigint auto_increment
        primary key,
    UserId     bigint         null,
    Latitude   decimal(10, 5) not null,
    Longitude  decimal(10, 5) not null,
    constraint Location_LocationId_uindex
        unique (LocationId),
    constraint UserId_fk
        foreign key (UserId) references User (UserId)
            on update cascade on delete cascade
);
create table Drivers
(
    DriversId bigint auto_increment
        primary key,
    DriversName     varchar(60) not null,
    DriversPhoneNumber varchar(10) not null,
    DriversMotorcycle   varchar(255) null,
    Latitude    decimal(10, 5) not null,
    Longitude   decimal(10, 5) not null,
    constraint Drivers_DriversId_uindex
        unique (DriversId)
);
create table `Order`
(
    OrderId                bigint auto_increment
        primary key,
    Latitude               decimal(10, 5) not null,
    Longitude              decimal(10, 5) not null,
    RestaurantId           bigint         not null,
    UserId                 bigint         not null,
    PaymentType            varchar(10)    not null,
    DriversName            varchar(50)    not null,
    DriversPhoneNumber     varchar(10)    null,
    DriversMotorcycleModel varchar(50)    not null,
    TitleOfFood            varchar(50)    not null,
    PriceOfFood            int            not null,
    SpecialRequirements    varchar(255)   null,
    AmountOrdered          int            not null,
    Completed              tinyint(1)     not null,
    constraint Order_OrderId_uindex
        unique (OrderId),
    constraint Order_RestaurantId_fk
        foreign key (RestaurantId) references Restaurant (RestaurantId)
            on update cascade on delete cascade,
    constraint Order_UserId_fk
        foreign key (UserId) references User (UserId)
            on update cascade on delete cascade
);