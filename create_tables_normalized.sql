START TRANSACTION;
CREATE DATABASE normalized_grabmyfood;
USE normalized_grabmyfood;
COMMIT;
create table User
(
    UserId      bigint auto_increment
        primary key,
    Username    varchar(60) not null,
    Password    varchar(60) not null,
    PhoneNumber varchar(10) not null,
    CreditCard  varchar(16) null,
    constraint User_UserId_uindex
        unique (UserId)
);
create table UserLocation
(
    UserLocationId bigint auto_increment
        primary key,
    Latitude       decimal(10, 5) not null,
    Longitude      decimal(10, 5) not null,
    constraint UserLocation_UserLocationId_uindex
        unique (UserLocationId)
);
create table UserLocationJunctionTable
(
    UserId         bigint not null,
    UserLocationId bigint not null,
    primary key (UserId, UserLocationId),
    constraint UserLocationJunctionTable_UserId_fk
        foreign key (UserId) references User (UserId)
            on update cascade on delete cascade,
    constraint UserLocationJunctionTable_UserLocationId_fk
        foreign key (UserLocationId) references UserLocation (UserLocationId)
            on update cascade on delete cascade
);
create table Drivers
(
    DriverId               bigint auto_increment
        primary key,
    Name                   varchar(60)  not null,
    PhoneNumber            varchar(10)  null,
    DriversMotorcycleModel varchar(255) not null,
    constraint Drivers_DriverId_uindex
        unique (DriverId)
);
create table DriversLocation
(
    DriverId bigint auto_increment
        primary key,
    Latitude       decimal(10, 5) not null,
    Longitude      decimal(10, 5) not null,
    constraint DriversLocation_DriversId_fk
        foreign key (DriverId) references Drivers (DriverId)
            on update cascade on delete cascade
);
create table Restaurant
(
    RestaurantId bigint auto_increment
        primary key,
    Name         varchar(60)  not null,
    PhoneNumber  varchar(10)  null,
    Branch       varchar(255) null,
    constraint Restaurant_RestaurantId_uindex
        unique (RestaurantId)
);
create table RestaurantLocation
(
    RestaurantId bigint auto_increment
        primary key,
    Latitude     decimal(10, 5) not null,
    Longitude    decimal(10, 5) not null,
    constraint RestaurantLocation_RestaurantId_fk
        foreign key (RestaurantId) references Restaurant (RestaurantId)
            on update cascade on delete cascade
);
create table RestaurantMenuListing
(
    RestaurantMenuListingId bigint auto_increment
        primary key,
    Title                   varchar(255) not null,
    Description             varchar(255) null,
    Price                   smallint     not null,
    constraint RestaurantMenuListing_RestaurantMenuListingId_uindex
        unique (RestaurantMenuListingId)
);
create table RestaurantMenuListingJunctionTable
(
    RestaurantId        bigint not null,
    RestaurantListingId bigint not null,
    primary key (RestaurantListingId, RestaurantId),
    constraint RestaurantMenuListingJunctionTable_RestaurantId_fk
        foreign key (RestaurantId) references Restaurant (RestaurantId)
            on update cascade on delete cascade,
    constraint RestaurantMenuListingJunctionTable_RestaurantMenuListingId_fk
        foreign key (RestaurantListingId) references RestaurantMenuListing (RestaurantMenuListingId)
            on update cascade on delete cascade
);
create table `Order`
(
    OrderId       bigint auto_increment
        primary key,
    UserLatitude  decimal(10, 5)              not null,
    UserLongitude decimal(10, 5)              not null,
    UserId        bigint                      not null,
    RestaurantId  bigint                      not null,
    DriverId      bigint                      not null,
    Completed     tinyint(1)                  not null,
    PaymentType   enum ('CreditCard', 'Cash') not null,
    constraint Order_OrderId_uindex
        unique (OrderId),
    constraint Order_DriverId_fk
        foreign key (DriverId) references Drivers (DriverId)
            on update cascade on delete cascade,
    constraint Order_RestaurantId_fk
        foreign key (RestaurantId) references Restaurant (RestaurantId)
            on update cascade on delete cascade,
    constraint Order_UserId_fk
        foreign key (UserId) references User (UserId)
            on update cascade on delete cascade
);
create table OrderListing
(
    OrderListingId      bigint auto_increment
        primary key,
    RestaurantListingId bigint,
    Amount              smallint     not null,
    SpecialRequirements varchar(255) null,
    constraint OrderListing_OrderListingId_uindex
        unique (OrderListingId),
    constraint OrderListing_RestaurantListingId_fk
        foreign key (RestaurantListingId) references RestaurantMenuListing (RestaurantMenuListingId)
            on update cascade on delete cascade
);
create table OrderListingJunctionTable
(
    OrderId        bigint not null,
    OrderListingId bigint not null,
    primary key (OrderId, OrderListingId),
    constraint OrderListingJunctionTable_OrderId_fk
        foreign key (OrderId) references `Order` (OrderId)
            on update cascade on delete cascade,
    constraint OrderListingJunctionTable_OrderListingId_fk
        foreign key (OrderListingId) references OrderListing (OrderListingId)
            on update cascade on delete cascade
);