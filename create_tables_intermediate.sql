START TRANSACTION;
CREATE DATABASE intermediate_grabmyfood;
USE intermediate_grabmyfood;
COMMIT;
create table Drivers
(
    DriverId        bigint auto_increment
        primary key,
    Name            varchar(60) not null,
    PhoneNumber     varchar(10) not null,
    MotorcycleModel varchar(60) null,
    Latitude   decimal(10, 5) not null,
    Longitude  decimal(10, 5) not null,
    constraint Drivers_DriverId_uindex
        unique (DriverId)
);
create table User
(
    UserId      bigint auto_increment
        primary key,
    Username    varchar(60) not null,
    Password    varchar(60) not null,
    PhoneNumber varchar(10) not null,
    CreditCard  varchar(16) null,
    constraint User_UserId_uindex
        unique (UserId)
);
create table Location
(
    LocationId bigint auto_increment
        primary key,
    UserId     bigint         null,
    Latitude   decimal(10, 5) not null,
    Longitude  decimal(10, 5) not null,
    constraint Location_LocationId_uindex
        unique (LocationId),
    constraint Location_UserId_fk
        foreign key (UserId) references User (UserId)
            on update cascade on delete cascade
);
create table RestaurantLocation
(
    RestaurantLocationId bigint auto_increment
        primary key,
    RestaurantId         bigint         not null,
    Latitude             decimal(10, 5) not null,
    Longitude            decimal(10, 5) not null,
    constraint RestaurantLocation_RestaurantLocationId_uindex
        unique (RestaurantLocationId)
);
create table RestaurantMenuListing
(
    RestaurantMenuListingId bigint auto_increment
        primary key,
    RestaurantId            bigint not null,
    ListingInformation      json   not null,
    constraint RestaurantMenuListing_RestaurantMenuListingId_uindex
        unique (RestaurantMenuListingId)
);
create table Restaurant
(
    RestaurantId         bigint auto_increment
        primary key,
    Name                 varchar(255) not null,
    PhoneNumber          varchar(10)  null,
    RestaurantListingId  bigint       not null,
    RestaurantLocationId bigint       not null,
    Branch               varchar(255) null,
    constraint Restaurant_RestaurantId_uindex
        unique (RestaurantId),
    constraint Restaurant_Uniqueness_uindex
        unique (Name,RestaurantLocationId,Branch),
    constraint Restaurant_ListingId_fk
        foreign key (RestaurantListingId) references RestaurantMenuListing (RestaurantMenuListingId)
            on update cascade on delete cascade,
    constraint Restaurant_LocationId_fk
        foreign key (RestaurantLocationId) references RestaurantLocation (RestaurantLocationId)
            on update cascade on delete cascade
);
create table `Order`
(
    OrderId       bigint auto_increment
        primary key,
    Completed     tinyint(1)                  null,
    UserLongitude decimal(10, 5)              not null,
    UserLatitude  decimal(10, 5)              not null,
    UserId        bigint                      not null,
    DriverId      bigint                      not null,
    OrderListing  json                        not null,
    RestaurantId  bigint                      not null,
    PaymentType   enum ('CreditCard', 'Cash') not null,
    constraint Order_OrderId_uindex
        unique (OrderId),
    constraint Order_DriverId_fk
        foreign key (DriverId) references Drivers (DriverId)
            on update cascade on delete cascade,
    constraint Order_RestaurantId_fk
        foreign key (RestaurantId) references Restaurant (RestaurantId)
            on update cascade on delete cascade,
    constraint Order_UserId_fk
        foreign key (UserId) references User (UserId)
            on update cascade on delete cascade
);