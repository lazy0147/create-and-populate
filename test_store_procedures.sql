USE normalized_grabmyfood;
# ----------------------------- CALLING CREATE INSTANCE PROCEDURES -----------------------------
CALL CreateUser("Test",
    "45a3s5d35a3sdf54",
    "0630630630",
    "1234567891011121");
CALL CreateUserLocation(1, 2.0, 3.0);
#------------------
# I spent 3 HOURS trying to make it so you could pass a JSON!
# FILE INTO THIS. I give up.
CALL CreateRestaurant("Fake", "1234567891", NULL);
CALL CreateRestaurantMenuListing(3,"New food", "Food Description", 1000);
#------------------
CALL CreateDriver("DriverBoyMan", "1800451324", "MK-X", -1, 1);
#------------------
CALL CreateOrder(6.0,8.0,5,4,3,TRUE,'CreditCard');
CALL CreateOrderListing(1,3, 100,NULL);


# ------------------ CALLING KEY BUSINESS FUNCTION PROCEDURES --------------------
CALL FindAllNearbyRestaurants(100.326063, 13.792813,1); # returns 1, 2
CALL GetRestaurantById(2); # we query and get 2
CALL FindAllNearbyDrivers(0.0,0.0,8000); # returns 3
CALL GetDriverById(1); # we query and get 1,2,3
CALL GetDriverById(2);
CALL GetDriverById(3);
CALL UpdateDriversLocation(1, 0, 0); # Sets 1 to be outside of radius
CALL FindAllNearbyDrivers(0.0,0.0,8000); # returns 2, 3
# Create an Order
CALL CreateOrder(0.0,0.0,1,1,1,FALSE,'Cash');
SELECT OrderId FROM `Order` WHERE Completed=FALSE AND PaymentType='Cash'; # This is the Id of the Order Created, 5
CALL UpdateOrderStatus(5, TRUE); # Set it to true
SELECT OrderId FROM `Order` WHERE Completed=FALSE AND PaymentType='Cash'; # Returns nothing now
SELECT OrderId FROM `Order` WHERE Completed=TRUE AND PaymentType='Cash'; # 5 and 2, the only 2 paid by 'Cash'