USE normalized_grabmyfood;
#------------------ USER STUFF ------------------------------
# Users
INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
VALUES ("Thomas", "PaWMvbUMb4bZnosFITjbpeYtqoGRQUZYiOO", "0452384632", "1234123412341234");
INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
VALUES ("Bromas", "l2sAtvbOjRUBPpt8uhG17nW8KQQbr2HwkTt", "0810810810", "1234567891011121");
INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
VALUES ("Pomas", "vylqIe3rZNuh2Dbck2igOuBz61mmhfFssrTw", "7777777777", "0258147369025812");
INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
VALUES ("Domas", "xFvNujTtzPvNq0SbjOcTEzE4LIRb8KVSpoIe", "7891236541", "0134679846513025");
INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
VALUES ("Cromas", "PaOzncUAjuOpqgXpF2To1OytI2tgYuOTVdk", "0147014702", "1346791346791346");
INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
VALUES ("Parris", "DYGjxr6OqOEByySle3Ujqy584rAgUBGmG9a", "0321654987", "3246897513450216");
# Location
INSERT INTO UserLocation (Latitude, Longitude)
VALUES (13.792813,100.326063);
INSERT INTO UserLocation (Latitude, Longitude)
VALUES (13.798312,100.328312);
INSERT INTO UserLocation (Latitude, Longitude)
VALUES (13.797062,100.312187);
INSERT INTO UserLocation (Latitude, Longitude)
VALUES (13.796812,100.311563);
INSERT INTO UserLocation (Latitude, Longitude)
VALUES (13.794313,100.312062);
INSERT INTO UserLocation (Latitude, Longitude)
VALUES (-78.993073, 31.792506);
INSERT INTO UserLocation (Latitude, Longitude)
VALUES (0.0, 0.0);
# UserLocationJunction
INSERT INTO UserLocationJunctionTable (UserId, UserLocationId)
VALUES (1,1);
INSERT INTO UserLocationJunctionTable (UserId, UserLocationId)
VALUES (2,2);
INSERT INTO UserLocationJunctionTable (UserId, UserLocationId)
VALUES (1,3);
INSERT INTO UserLocationJunctionTable (UserId, UserLocationId)
VALUES (4,4);
INSERT INTO UserLocationJunctionTable (UserId, UserLocationId)
VALUES (5,5);
INSERT INTO UserLocationJunctionTable (UserId, UserLocationId)
VALUES (3,6);
INSERT INTO UserLocationJunctionTable (UserId, UserLocationId)
VALUES (6,7);
#------------------ RESTAURANT STUFF ------------------------------
# Restaurant && RestaurantLocation
# - Done in order like this so that we dont have to set the RestaurantId Manually.
# - Although it could be done (i.e. INSERT INTO RestaurantLocation (RestaurantId, Latitude, Longitude) ... )
INSERT INTO Restaurant (Name, PhoneNumber, Branch)
VALUES ("Thomas'es BBQ", "0452384632",NULL);
INSERT INTO RestaurantLocation (Latitude, Longitude)
VALUES (13.79981,100.32756);
INSERT INTO Restaurant (Name, PhoneNumber, Branch)
VALUES ("WcDonalds", "1456321457","Chiang Mai");
INSERT INTO RestaurantLocation (Latitude, Longitude)
VALUES (13.79731,100.33031);
INSERT INTO Restaurant (Name, PhoneNumber, Branch)
VALUES ("WcDonalds", "1456321457","Bangkok");
INSERT INTO RestaurantLocation (Latitude, Longitude)
VALUES (13.80206,100.31931);
# RestaurantMenuListing
INSERT INTO RestaurantMenuListing (Title, Description, Price)
VALUES ("Burger", "Its a burger idk", 100);
INSERT INTO RestaurantMenuListing (Title, Description, Price)
VALUES ("Fries from the french", "Its french fries but its not french", 100);
INSERT INTO RestaurantMenuListing (Title, Description, Price)
VALUES ("CokaCola", "Coke and Cola", 15);
INSERT INTO RestaurantMenuListing (Title, Description, Price)
VALUES ("The WcBurger", "WcDonalds Classic WcBurger", 150);
INSERT INTO RestaurantMenuListing (Title, Description, Price)
VALUES ("The WcFries", "WcDonalds Classic WcFries", 100);
INSERT INTO RestaurantMenuListing (Title, Description, Price)
VALUES ("All Star WcBox", "WcFries and WcBurger", 200);
INSERT INTO RestaurantMenuListing (Title, Description, Price)
VALUES ("The Destruction Of Mankind", "This is not a joke", 200);
# RestaurantMenuJunctionTable
INSERT INTO RestaurantMenuListingJunctionTable (RestaurantId, RestaurantListingId)
VALUES (1, 1);
INSERT INTO RestaurantMenuListingJunctionTable (RestaurantId, RestaurantListingId)
VALUES (1, 2);
INSERT INTO RestaurantMenuListingJunctionTable (RestaurantId, RestaurantListingId)
VALUES (1, 3);
INSERT INTO RestaurantMenuListingJunctionTable (RestaurantId, RestaurantListingId)
VALUES (2, 4);
INSERT INTO RestaurantMenuListingJunctionTable (RestaurantId, RestaurantListingId)
VALUES (2, 5);
INSERT INTO RestaurantMenuListingJunctionTable (RestaurantId, RestaurantListingId)
VALUES (2, 6);
INSERT INTO RestaurantMenuListingJunctionTable (RestaurantId, RestaurantListingId)
VALUES (2, 7);
INSERT INTO RestaurantMenuListingJunctionTable (RestaurantId, RestaurantListingId)
VALUES (3, 4);
INSERT INTO RestaurantMenuListingJunctionTable (RestaurantId, RestaurantListingId)
VALUES (3, 5);
INSERT INTO RestaurantMenuListingJunctionTable (RestaurantId, RestaurantListingId)
VALUES (3, 6);
INSERT INTO RestaurantMenuListingJunctionTable (RestaurantId, RestaurantListingId)
VALUES (3, 7);
#------------------ DRIVER STUFF ------------------------------
INSERT INTO Drivers (Name, PhoneNumber, DriversMotorcycleModel)
VALUES ("DriverMan", "1203164789", "Kawasaki Ninja 400");
INSERT INTO Drivers (Name, PhoneNumber, DriversMotorcycleModel)
VALUES ("DriverBoy", "4513461254", "Meteor 350");
# Divers Location
INSERT INTO DriversLocation (Latitude, Longitude)
VALUES (13.80206,100.31931);
INSERT INTO DriversLocation (Latitude, Longitude)
VALUES (13.792813,100.326063);
#------------------ ORDER STUFF ------------------------------
# Order
INSERT INTO `Order` (UserLatitude, UserLongitude, UserId, RestaurantId, DriverId, Completed, PaymentType)
VALUES (13.792813,100.326063,1,1,1,TRUE,'CreditCard');
INSERT INTO `Order` (UserLatitude, UserLongitude, UserId, RestaurantId, DriverId, Completed, PaymentType)
VALUES (13.798312,100.328312,3,2,1,TRUE,'Cash');
INSERT INTO `Order` (UserLatitude, UserLongitude, UserId, RestaurantId, DriverId, Completed, PaymentType)
VALUES (13.796812,100.311563,4,3,2,FALSE,'CreditCard');
# OrderListing
INSERT INTO OrderListing (RestaurantListingId, Amount, SpecialRequirements)
VALUES (1, 3, "Not Spicy");
INSERT INTO OrderListing (RestaurantListingId, Amount, SpecialRequirements)
VALUES (2, 3, NULL);
INSERT INTO OrderListing (RestaurantListingId, Amount, SpecialRequirements)
VALUES (3, 3, NULL);
INSERT INTO OrderListing (RestaurantListingId, Amount, SpecialRequirements)
VALUES (7, 1, "Mild Please");
INSERT INTO OrderListing (RestaurantListingId, Amount, SpecialRequirements)
VALUES (7, 1, "Mild Please");
INSERT INTO OrderListing (RestaurantListingId, Amount, SpecialRequirements)
VALUES (6, 1, NULL);
# OrderListingJunctionTable
INSERT INTO OrderListingJunctionTable (OrderId, OrderListingId)
VALUES (1,1);
INSERT INTO OrderListingJunctionTable (OrderId, OrderListingId)
VALUES (1,2);
INSERT INTO OrderListingJunctionTable (OrderId, OrderListingId)
VALUES (1,3);
INSERT INTO OrderListingJunctionTable (OrderId, OrderListingId)
VALUES (2,4);
INSERT INTO OrderListingJunctionTable (OrderId, OrderListingId)
VALUES (3,5);
INSERT INTO OrderListingJunctionTable (OrderId, OrderListingId)
VALUES (3,6);