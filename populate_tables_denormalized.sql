USE denormalized_grabmyfood;
# Users
INSERT INTO User (Username, Password, Phone_Number, CreditCard)
VALUES ("Thomas", "PaWMvbUMb4bZnosFITjbpeYtqoGRQUZYiOO", "0452384632", "1234123412341234");
INSERT INTO User (Username, Password, Phone_Number, CreditCard)
VALUES ("Bromas", "l2sAtvbOjRUBPpt8uhG17nW8KQQbr2HwkTt", "0810810810", "1234567891011121");
INSERT INTO User (Username, Password, Phone_Number, CreditCard)
VALUES ("Pomas", "vylqIe3rZNuh2Dbck2igOuBz61mmhfFssrTw", "7777777777", "0258147369025812");
INSERT INTO User (Username, Password, Phone_Number, CreditCard)
VALUES ("Domas", "xFvNujTtzPvNq0SbjOcTEzE4LIRb8KVSpoIe", "7891236541", "0134679846513025");
INSERT INTO User (Username, Password, Phone_Number, CreditCard)
VALUES ("Cromas", "PaOzncUAjuOpqgXpF2To1OytI2tgYuOTVdk", "0147014702", "1346791346791346");
INSERT INTO User (Username, Password, Phone_Number, CreditCard)
VALUES ("Parris", "DYGjxr6OqOEByySle3Ujqy584rAgUBGmG9a", "0321654987", "3246897513450216");
# Location
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (1,13.792813,100.326063);
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (1, 13.798312,100.328312);
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (2, 13.797062,100.312187);
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (2, 13.796812,100.311563);
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (2, 13.794313,100.312062);
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (5, -78.993073, 31.792506);
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (6, 0.0, 0.0);
# Drivers
INSERT INTO Drivers (DriversName , DriversPhoneNumber, DriversMotorcycle, Latitude, Longitude)
VALUES ("DriverMan", "1203164789", "Kawasaki Ninja 400", 13.80206,100.31931);
INSERT INTO Drivers (DriversName , DriversPhoneNumber, DriversMotorcycle, Latitude, Longitude)
VALUES ("DriverBoy", "4513461254", "Meteor 350", 13.792813,100.326063);
# Restaurant
INSERT INTO Restaurant (Name, PhoneNumber, TitleOfListing, DescriptionOfListing, PriceOfListing, Latitude, Longitude, Branch)
VALUES ("Thomas'es BBQ", "0452384632", "Burger", "Its a burger idk", 100, 13.792813,100.326063, NULL);
INSERT INTO Restaurant (Name, PhoneNumber, TitleOfListing, DescriptionOfListing, PriceOfListing, Latitude, Longitude, Branch)
VALUES ("Thomas'es BBQ", "0452384632", "Fries from the french", "French fries but they're not french", 100, 13.792813,100.326063, NULL);
INSERT INTO Restaurant (Name, PhoneNumber, TitleOfListing, DescriptionOfListing, PriceOfListing, Latitude, Longitude, Branch)
VALUES ("Thomas'es BBQ", "0452384632", "CokaCola", "Coke and Cola", 15, 13.792813,100.326063, NULL);
INSERT INTO Restaurant (Name, PhoneNumber, TitleOfListing, DescriptionOfListing, PriceOfListing, Latitude, Longitude, Branch)
VALUES ("WcDonalds", "1456321457", "The WcBurger", "WcDonalds Classic WcBurger", 150, 13.798312,100.328312, "Chiang Mai");
INSERT INTO Restaurant (Name, PhoneNumber, TitleOfListing, DescriptionOfListing, PriceOfListing, Latitude, Longitude, Branch)
VALUES ("WcDonalds", "1456321457", "The WcFries", "WcDonalds Classic WcFries", 100, 13.798312,100.328312, "Chiang Mai");
INSERT INTO Restaurant (Name, PhoneNumber, TitleOfListing, DescriptionOfListing, PriceOfListing, Latitude, Longitude, Branch)
VALUES ("WcDonalds", "1456321457", "All Star WcBox", "WcFries and WcBurger", 200, 13.798312,100.328312, "Chiang Mai");
INSERT INTO Restaurant (Name, PhoneNumber, TitleOfListing, DescriptionOfListing, PriceOfListing, Latitude, Longitude, Branch)
VALUES ("WcDonalds", "1456321457", "The Destruction Of Mankind", "This is not a joke", 200, 13.798312,100.328312, "Chiang Mai");
INSERT INTO Restaurant (Name, PhoneNumber, TitleOfListing, DescriptionOfListing, PriceOfListing, Latitude, Longitude, Branch)
VALUES ("WcDonalds", "1456321457", "The WcBurger", "WcDonalds Classic WcBurger", 150, 13.796812,100.311563, "Bangkok");
INSERT INTO Restaurant (Name, PhoneNumber, TitleOfListing, DescriptionOfListing, PriceOfListing, Latitude, Longitude, Branch)
VALUES ("WcDonalds", "1456321457", "The WcFries", "WcDonalds Classic WcFries", 100, 13.796812,100.3115630, "Bangkok");
INSERT INTO Restaurant (Name, PhoneNumber, TitleOfListing, DescriptionOfListing, PriceOfListing, Latitude, Longitude, Branch)
VALUES ("WcDonalds", "1456321457", "All Star WcBox", "WcFries and WcBurger", 200, 13.796812,100.311563, "Bangkok");
INSERT INTO Restaurant (Name, PhoneNumber, TitleOfListing, DescriptionOfListing, PriceOfListing, Latitude, Longitude, Branch)
VALUES ("WcDonalds", "1456321457", "The Destruction Of Mankind", "This is not a joke", 200, 13.796812,100.311563, "Bangkok");
# Order
INSERT INTO `Order` (Latitude, Longitude, RestaurantId, UserId, PaymentType, DriversName, DriversPhoneNumber, DriversMotorcycleModel, TitleOfFood, PriceOfFood, SpecialRequirements, AmountOrdered, Completed)
VALUES (13.796812,100.311563, 4, 1, "CreditCard", "DriverMan", "1203164789", "Kawasaki Ninja 400", "The Destruction Of Mankind", 200, "Not Spicy Please", 2, FALSE);
INSERT INTO `Order` (Latitude, Longitude, RestaurantId, UserId, PaymentType, DriversName, DriversPhoneNumber, DriversMotorcycleModel, TitleOfFood, PriceOfFood, SpecialRequirements, AmountOrdered, Completed)
VALUES (13.796812,100.311563, 4, 1, "CreditCard", "DriverMan", "1203164789", "Kawasaki Ninja 400", "All Star WcBox", 200, "Not Spicy Please", 2, TRUE);
INSERT INTO `Order` (Latitude, Longitude, RestaurantId, UserId, PaymentType, DriversName, DriversPhoneNumber, DriversMotorcycleModel, TitleOfFood, PriceOfFood, SpecialRequirements, AmountOrdered, Completed)
VALUES (13.792813,100.326063, 1, 1, "CreditCard", "DriverBoy", "4513461254", "Meteor 350", "CokaCola", 15, NULL, 10, TRUE);
INSERT INTO `Order` (Latitude, Longitude, RestaurantId, UserId, PaymentType, DriversName, DriversPhoneNumber, DriversMotorcycleModel, TitleOfFood, PriceOfFood, SpecialRequirements, AmountOrdered, Completed)
VALUES (13.792813,100.326063, 1, 1, "CreditCard", "DriverBoy", "4513461254", "Meteor 350", "Burger", 100, NULL, 10, TRUE);
INSERT INTO `Order` (Latitude, Longitude, RestaurantId, UserId, PaymentType, DriversName, DriversPhoneNumber, DriversMotorcycleModel, TitleOfFood, PriceOfFood, SpecialRequirements, AmountOrdered, Completed)
VALUES (13.792813,100.326063, 1, 1, "CreditCard", "DriverBoy", "4513461254", "Meteor 350", "Fries from the french", 100, NULL, 10, TRUE);