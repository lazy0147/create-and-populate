USE normalized_grabmyfood;
# ------------USER-------------
# Create a User
DROP PROCEDURE IF EXISTS CreateUser;
DELIMITER //
CREATE PROCEDURE CreateUser(
    InputUsername VARCHAR(60),
    InputPassword VARCHAR(60),
    InputPhoneNumber VARCHAR(10),
    InputCreditCard VARCHAR(16)
)
BEGIN
     INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
         VALUES (InputUsername, InputPassword, InputPhoneNumber, InputCreditCard);
END//
DELIMITER ;
# User Location
DROP PROCEDURE IF EXISTS CreateUserLocation;
DELIMITER //
CREATE PROCEDURE CreateUserLocation(
    InputUserId BIGINT,
    InputLatitude DECIMAL(10,5),
    InputLongitude DECIMAL(10,5)
)
BEGIN
    INSERT INTO UserLocation (Latitude, Longitude)
        VALUES (InputLatitude, InputLongitude);
    INSERT INTO UserLocationJunctionTable (UserId, UserLocationId)
        VALUES (InputUserId, (SELECT MAX(UserLocationId) FROM UserLocation));
END //
DELIMITER ;
# ------------RESTAURANT-------------
# CreateRestaurant
DROP PROCEDURE IF EXISTS CreateRestaurant;
DELIMITER xx
CREATE PROCEDURE CreateRestaurant(
    InputName VARCHAR(60),
    InputPhoneNumber VARCHAR(60),
    InputBranch VARCHAR(255)
)
BEGIN
    INSERT INTO Restaurant (Name, PhoneNumber, Branch)
        VALUES (InputName, InputPhoneNumber, InputBranch);
END xx
DELIMITER ;
# RestaurantMenuListing
DROP PROCEDURE IF EXISTS CreateRestaurantMenuListing;
DELIMITER xx
CREATE PROCEDURE CreateRestaurantMenuListing(
    InputRestaurantId BIGINT,
    InputTitle VARCHAR(255),
    InputDescription VARCHAR(255),
    InputPrice SMALLINT
)
BEGIN
    INSERT INTO RestaurantMenuListing (Title, Description, Price)
        VALUES (InputTitle, InputDescription, InputPrice);
    INSERT INTO RestaurantMenuListingJunctionTable (RestaurantId, RestaurantListingId)
        VALUES (InputRestaurantId,(SELECT MAX(RestaurantMenuListingId) FROM RestaurantMenuListing));
END xx
DELIMITER ;
# RestaurantLocation
DROP PROCEDURE IF EXISTS CreateRestaurantLocation;
DELIMITER xx
CREATE PROCEDURE CreateRestaurantLocation(
    InputRestaurantId BIGINT,
    RestaurantLatitude DECIMAL(10,5),
    RestaurantLongitude DECIMAL(10,5)
) BEGIN
    INSERT INTO RestaurantLocation (RestaurantId, Latitude, Longitude)
        VALUES (InputRestaurantId, RestaurantLatitude, RestaurantLongitude);
END xx
DELIMITER ;
# ------------DRIVER-------------
DROP PROCEDURE IF EXISTS CreateDriver;
DELIMITER xx
CREATE PROCEDURE CreateDriver(
    InputName VARCHAR(60),
    InputPhoneNumber VARCHAR(10),
    InputMotorcycle VARCHAR(255),
    InputLatitude DECIMAL(10,5),
    InputLongitude DECIMAL(10,5)
) BEGIN
    IF (InputLatitude BETWEEN -90 AND 90) AND (InputLongitude BETWEEN -180 AND 180) THEN
        INSERT INTO Drivers (Name, PhoneNumber, DriversMotorcycleModel)
            VALUES (InputName, InputPhoneNumber, InputMotorcycle);
        INSERT INTO DriversLocation (Latitude, Longitude)
            VALUES (InputLatitude, InputLongitude);
    ELSE
        SIGNAL SQLSTATE '22003'
            SET MESSAGE_TEXT = 'Latitude/longitude exceeds the range (-90 <= latitude <= 90, -180 <= longitude <= 180).';
    END IF;
END xx
DELIMITER ;
# ------------ORDER-------------
DROP PROCEDURE IF EXISTS CreateOrder;
DELIMITER xx
CREATE PROCEDURE CreateOrder(
    Latitude DECIMAL(10,5),
    Longitude DECIMAL(10,5),
    InputUserId BIGINT,
    InputRestaurantId BIGINT,
    InputDriversId BIGINT,
    InputCompleted BOOLEAN,
    InputPaymentType ENUM ('CreditCard','Cash')
) BEGIN
    IF (Latitude BETWEEN -90 AND 90) AND (Longitude BETWEEN -180 AND 180) THEN
        INSERT INTO `Order` (UserLatitude, UserLongitude, UserId, RestaurantId, DriverId, Completed, PaymentType)
            VALUES (Latitude,Longitude,InputUserId,InputRestaurantId,InputDriversId,InputCompleted,InputPaymentType);
    ELSE
        SIGNAL SQLSTATE '22003'
            SET MESSAGE_TEXT = 'Latitude/longitude exceeds the range (-90 <= latitude <= 90, -180 <= longitude <= 180).';
    END IF;
END xx
DELIMITER ;
# Create OrderListing
DROP PROCEDURE IF EXISTS CreateOrderListing;
DELIMITER xx
CREATE PROCEDURE CreateOrderListing(
    InputRestaurantListingId BIGINT,
    InputOrderId BIGINT,
    InputAmount SMALLINT,
    InputSpecialRequirements VARCHAR(255)
) BEGIN
    INSERT INTO OrderListing (RestaurantListingId, Amount, SpecialRequirements)
        VALUES (InputRestaurantListingId,InputAmount,InputSpecialRequirements);
    INSERT INTO OrderListingJunctionTable (OrderId, OrderListingId)
        VALUES (InputOrderId,(SELECT MAX(OrderListingId) FROM OrderListing));
END xx
DELIMITER ;


# ----------------------------- KEY BUSINESS FUNCTIONS -----------------------------
# -------------- FUNCTION -------------------
DROP FUNCTION IF EXISTS GREAT_CIRCLE_DISTANCE;
CREATE FUNCTION GREAT_CIRCLE_DISTANCE(lat1 decimal(10, 5), lng1 decimal(10, 5), lat2 decimal(10, 5), lng2 decimal(10, 5))
RETURNS decimal(10, 5)
DETERMINISTIC
BEGIN
    # Compute distance in km
    # Source: https://web.archive.org/web/20170126150533/https://developers.google.com/maps/articles/phpsqlsearch_v3
    RETURN ( 6371 * acos( cos( radians(lat1) ) * cos( radians( lat2 ) ) * cos( radians( lng2 ) - radians(lng1) ) + sin( radians(lat1) ) * sin( radians( lat2 ) ) ) );
END;
# -------------- GETTING -------------------
# Get Restaurant
DROP PROCEDURE IF EXISTS GetRestaurantById;
DELIMITER //
CREATE PROCEDURE GetRestaurantById(
    InputId BIGINT
) BEGIN
    SELECT * FROM Restaurant WHERE RestaurantId = InputId;
END //
DELIMITER ;
# Get Driver
DROP PROCEDURE IF EXISTS GetDriverById;
DELIMITER //
CREATE PROCEDURE GetDriverById(
    InputId BIGINT
) BEGIN
    SELECT * FROM Drivers WHERE DriverId = InputId;
END //
DELIMITER ;
# -------------- UPDATING -------------------
# Driver Location Updates
DROP PROCEDURE IF EXISTS UpdateDriversLocation;
DELIMITER xx
CREATE PROCEDURE UpdateDriversLocation(
    InputDriverId BIGINT,
    InputLatitude DECIMAL(10, 5),
    InputLongitude DECIMAL(10, 5)
) BEGIN
    UPDATE DriversLocation SET Longitude = InputLongitude, Latitude = InputLatitude WHERE DriverId = InputDriverId;
END xx
DELIMITER ;
# Order Status Updates
DROP PROCEDURE IF EXISTS UpdateOrderStatus;
DELIMITER xx
CREATE PROCEDURE UpdateOrderStatus(
    InputOrderId BIGINT,
    InputStatus BOOL
) BEGIN
    UPDATE `Order` SET Completed = InputStatus WHERE OrderId = InputOrderId;
END xx
DELIMITER ;
# -------------- SCANNING -------------------
# Scan for nearby restaurants
DROP PROCEDURE IF EXISTS FindAllNearbyRestaurants;
DELIMITER //
CREATE PROCEDURE FindAllNearbyRestaurants(
    InputLongitude DECIMAL(10,5),
    InputLatitude DECIMAL(10,5),
    Radius SMALLINT
) BEGIN
    SELECT RestaurantId
    FROM
         (
             SELECT RestaurantId,
                    GREAT_CIRCLE_DISTANCE(Latitude, Longitude, InputLatitude, InputLongitude) distance
             FROM RestaurantLocation
         ) RestaurantsDistance
    WHERE distance <= Radius;
END //
DELIMITER ;
# Scan for nearby drivers
DROP PROCEDURE IF EXISTS FindAllNearbyDrivers;
DELIMITER //
CREATE PROCEDURE FindAllNearbyDrivers(
    InputLongitude DECIMAL(10,5),
    InputLatitude DECIMAL(10,5),
    Radius SMALLINT
) BEGIN
    SELECT driverId
    FROM
         (
             SELECT DriverId,
                    GREAT_CIRCLE_DISTANCE(Latitude, Longitude, InputLatitude, InputLongitude) distance
             FROM DriversLocation
         ) DriversDistance
    WHERE distance <= Radius;
END //
DELIMITER ;
