USE intermediate_grabmyfood;
# Users
INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
VALUES ("Thomas", "PaWMvbUMb4bZnosFITjbpeYtqoGRQUZYiOO", "0452384632", "1234123412341234");
INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
VALUES ("Bromas", "l2sAtvbOjRUBPpt8uhG17nW8KQQbr2HwkTt", "0810810810", "1234567891011121");
INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
VALUES ("Pomas", "vylqIe3rZNuh2Dbck2igOuBz61mmhfFssrTw", "7777777777", "0258147369025812");
INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
VALUES ("Domas", "xFvNujTtzPvNq0SbjOcTEzE4LIRb8KVSpoIe", "7891236541", "0134679846513025");
INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
VALUES ("Cromas", "PaOzncUAjuOpqgXpF2To1OytI2tgYuOTVdk", "0147014702", "1346791346791346");
INSERT INTO User (Username, Password, PhoneNumber, CreditCard)
VALUES ("Parris", "DYGjxr6OqOEByySle3Ujqy584rAgUBGmG9a", "0321654987", "3246897513450216");
# Location
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (1,13.792813,100.326063);
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (1, 13.798312,100.328312);
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (2, 13.797062,100.312187);
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (2, 13.796812,100.311563);
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (2, 13.794313,100.312062);
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (5, -78.993073, 31.792506);
INSERT INTO Location (UserId, Latitude, Longitude)
VALUES (6, 0.0, 0.0);
# Drivers
INSERT INTO Drivers (Name, PhoneNumber, MotorcycleModel, Latitude, Longitude)
VALUES ("DriverMan", "1203164789", "Kawasaki Ninja 400", 13.80206,100.31931);
INSERT INTO Drivers (Name, PhoneNumber, MotorcycleModel, Latitude, Longitude)
VALUES ("DriverBoy", "4513461254", "Meteor 350", 13.792813,100.326063);
# Restaurant Location
INSERT INTO RestaurantLocation (RestaurantId, Latitude, Longitude)
VALUES (1, 13.792813,100.326063);
INSERT INTO RestaurantLocation (RestaurantId, Latitude, Longitude)
VALUES (2, 13.798312,100.328312);
INSERT INTO RestaurantLocation (RestaurantId, Latitude, Longitude)
VALUES (3, 13.796812,100.311563);
# Restaurant Listing
INSERT INTO RestaurantMenuListing (RestaurantId, ListingInformation)
VALUES (1,
        '
        {
            "Listing": [
                {
                  "Title": "Burger",
                  "Description": "Its a burger idk",
                  "Price": 100
                },
                {
                  "Title": "Fries from the french",
                  "Description": "Its french fries but its not french",
                  "Price": 100
                },
                {
                  "Title": "CokaCola",
                  "Description": "Coke and Cola",
                  "Price": 15
                }
            ]
        }
        ');
INSERT INTO RestaurantMenuListing (RestaurantId, ListingInformation)
VALUES (2,
        '
        {
            "Listing": [
                {
                  "Title": "The WcBurger",
                  "Description": "WcDonalds Classic WcBurger",
                  "Price": 150
                },
                {
                  "Title": "The WcFries",
                  "Description": "WcDonalds Classic WcFries",
                  "Price": 100
                },
                {
                  "Title": "All Star WcBox",
                  "Description": "WcFries and WcBurger",
                  "Price": 200
                },
                {
                  "Title": "The Destruction Of Mankind",
                  "Description": "This is not a joke",
                  "Price": 200
                }
            ]
        }
        ');
# Restaurant
INSERT INTO Restaurant (Name, PhoneNumber, RestaurantListingId, RestaurantLocationId, Branch)
VALUES ("Thomas'es BBQ", "0452384632", 1, 1, NULL);
INSERT INTO Restaurant (Name, PhoneNumber, RestaurantListingId, RestaurantLocationId, Branch)
VALUES ("WcDonalds", "1456321457", 2, 2, "Chiang Mai");
INSERT INTO Restaurant (Name, PhoneNumber, RestaurantListingId, RestaurantLocationId, Branch)
VALUES ("WcDonalds", "1456321457", 2, 3, "Bangkok");
# Order
INSERT INTO `Order` (Completed, UserLongitude, UserLatitude, UserId, DriverId, OrderListing, RestaurantId, PaymentType)
VALUES (TRUE, 13.792813,100.326063, 1, 1,
        '{
          "Listing":[
            {
              "Title": "Burger",
              "Amount": 3,
              "SpecialRequirements": "Not Spicy"
            },
            {
              "Title": "Fries from the french",
              "Amount": 3
            },
            {
              "Title": "CokaCola",
              "Amount": 3
            }
          ]
        }', 1, 'CreditCard');
INSERT INTO `Order` (Completed, UserLongitude, UserLatitude, UserId, DriverId, OrderListing, RestaurantId, PaymentType)
VALUES (FALSE, 13.798312,100.328312, 3, 1,
        '{
          "Listing":[
            {
              "Title": "The Destruction Of Mankind",
              "Amount": 1,
              "SpecialRequirements": "Mild Please"
            }
          ]
        }', 2, 'Cash');
INSERT INTO `Order` (Completed, UserLongitude, UserLatitude, UserId, DriverId, OrderListing, RestaurantId, PaymentType)
VALUES (FALSE, 13.796812,100.311563, 4, 1,
        '{
          "Listing":[
            {
              "Title": "The Destruction Of Mankind",
              "Amount": 1,
              "SpecialRequirements": "Mild Please"
            },
            {
              "Title": "All Star WcBox",
              "Amount": 1
            }
          ]
        }', 3, 'CreditCard');